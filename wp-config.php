<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'whybe');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'welcome');

/** MySQL hostname */
define('DB_HOST', '18.216.110.73');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@%/j-}QBf3}YA}ED)q*!ptfSY<SxM<29!Iv(6vZ*/>fIi$c0UX@7,H?eG-T.TDk,');
define('SECURE_AUTH_KEY',  'ykXxaDmA3,I(*iiw(_JWBbW8c9CBvo(;Y`fQe#.n@7C.HI[6xXBN/+(pAcuVqL0)');
define('LOGGED_IN_KEY',    'QACQB=[M-(RkO[Y}aqyzc}CH:KZ3KS.!N4y/]Kg;[obp=d3pTFtw-]/^_oJois2Q');
define('NONCE_KEY',        '(v+Vgk-jc6{-$a*-g3(m`X1WFq5>+YbT7Ep_G|lod&z4A~AXvK_kAIO21;&/Kn#S');
define('AUTH_SALT',        'f;U0:^`cJh<#54_RVQ~E1,EM~k0G.`i@7U&m]GvR+q80U7@=t6$p:T~iZZg?%EMX');
define('SECURE_AUTH_SALT', 'I1+-6}[y/!-n{GCGj=]0F]x@!;S!&#ouwk~<P~dijafk.kg.B0BC$?_v[L&mpQ|c');
define('LOGGED_IN_SALT',   '<FLn${*9<Dd1)Gy#c;k=YC)iH,8>}*j*]9d6`j9S`2p&e/>m}}> OD[fX/pDK=y!');
define('NONCE_SALT',       '-GMCP~{kRg(-:syxK?X}44+z?.g%d/j?v8)`iPnyfAm{3%2wIzEw6VfQ%2U(kTWW');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wb_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

